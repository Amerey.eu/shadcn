import { CustomTable } from "@/components/custom-table"
import { CustomCard } from "@/components/custom-card"
import { CardWithForm } from "@/components/custom-card2"

export default function IndexPage() {
  return (
    <section className="container grid items-center gap-6 pb-8 pt-6 md:py-10">
      <div className="flex flex-column items-start gap-4">
        <h2>Custom Table</h2>
        <CustomTable />

        <h2>Custom Card</h2>
        <CustomCard />
    
        <h2>Custom Card with form</h2>
        <CardWithForm />
      </div>
    </section>
  )
}

